public class ArrayDeletionsLab {

    public static void main(String[] args) {
        int[] number = {1,2,3,4,5};
        int deleteIndex = 2;
        int Value = 4;
        int[] newArr = deleteElementByIndex(number, deleteIndex);
        System.out.print("Array Origin : ");
        printArr(number);
        System.out.print("Array after deleting element at index "+deleteIndex+" : ");
        printArr(newArr);
        System.out.print("Array after deleting element with value "+Value+" : ");
        newArr = deleteElementByValue(newArr, Value);
        printArr(newArr);
        
    }
    public static int[] deleteElementByIndex(int[] arr, int deleteindex){
        int[] newArr = new int[arr.length - 1];
        for(int i=0;i<deleteindex;i++){
            newArr[i] = arr[i]; 
        }
        for(int i=deleteindex + 1;i<arr.length;i++){
            newArr[i-1] = arr[i];
        }
        return newArr;
    }
    public static int[] deleteElementByValue(int[] arr, int value){
        int[] newArr = new int[arr.length - 1];
        for (int i=0;i<arr.length;i++){
            if(arr[i] == value){
                for(int j=0;j<i;j++){
                    newArr[j] = arr[j]; 
                }
                for(int j = i + 1;j<arr.length;j++){
                        newArr[j-1] = arr[j];
                    }
            }
        }
        return newArr;
    }
    public static void printArr(int[] arr){
        System.out.print("[");
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]);
            if(i<arr.length-1){
                System.out.print(",");
            }
        }
        System.out.println("]");
        
    }
}
